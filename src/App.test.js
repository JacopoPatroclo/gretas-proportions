import React from "react";
import { render } from "@testing-library/react";
import { parseRecepy } from "./App";

test("parse the recepy", () => {
  const recepy = `174 g di farina integrale
145g lievito madre rinfrescato
124 g d'acqua
5 g di sale fino`;

  expect(parseRecepy(recepy)).toEqual([
    { tot: 174, name: "di farina integrale", unit: "g", error: null },
    { tot: 145, name: "lievito madre rinfrescato", unit: "g", error: null },
    { tot: 124, name: "d'acqua", unit: "g", error: null },
    { tot: 5, name: "di sale fino", unit: "g", error: null },
  ]);
});
