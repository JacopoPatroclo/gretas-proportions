import React from "react";
import "./App.css";
import {
  TextField,
  Grid,
  Container,
  Typography,
  FormControl,
  Card,
  CardContent,
  makeStyles,
  Select,
  MenuItem,
  Switch,
} from "@material-ui/core";

export const parseRecipe = (text = "") => {
  const parseLine = (textLine = "") => {
    // eslint-disable-next-line
    const regexp = /^([0-9]*)\s?(k?g)\ ([\w|\ |'|\(|\)]*)$/gm;
    let exOut = [];
    let res;
    while ((res = regexp.exec(textLine))) {
      exOut = res;
    }
    let error = null;
    const tot = exOut.length >= 2 ? Number(exOut[1]) : (error = "tot");
    const unit = exOut.length >= 3 ? exOut[2] : (error = "unit");
    const name = exOut.length >= 4 ? exOut[3] : (error = "name");
    return { tot, unit, name, error };
  };
  const lines = text.split("\n");
  return lines.filter((line) => line.trim() !== "").map(parseLine);
};

const ingridientToStr = (ingridients = []) => {
  return ingridients
    .map(
      (ingridient) => `${ingridient.tot} ${ingridient.unit} ${ingridient.name}`
    )
    .join("\n");
};

const useStyles = makeStyles((theme) => ({
  big: {
    "&": {
      margin: theme.spacing(1),
      width: "24ch",
    },
  },
  small: {
    "&": {
      margin: theme.spacing(1),
      width: "6ch",
    },
  },
}));

let lastPivot;

function App() {
  const classes = useStyles();
  const [recipe, setRecipe] = React.useState("");
  const [ingridients, setIngridients] = React.useState([]);
  const [pivot, setPivot] = React.useState(null);

  const chageTextarea = (e) => {
    setRecipe(e.target.value);
    setIngridients(parseRecipe(e.target.value));
  };

  const updateIndexIngridient = (index, key, val) => {
    const copy = [...ingridients];
    if (copy[index].error && copy[index].error === key) {
      copy[index].error = null;
    }
    const noError = copy.reduce((acc, item) => acc && !item.error, true);
    copy[index][key] = val;
    setIngridients(copy);
    if (noError && !pivot) {
      setRecipe(ingridientToStr(copy));
    }
  };

  const renderWithPivot = (current) => {
    if (lastPivot && pivot) {
      const lastPivotTot = lastPivot.tot;
      const newPivotTot = pivot.tot;
      return Number((current * newPivotTot) / lastPivotTot).toFixed(0);
    } else {
      return current;
    }
  };

  return (
    <Container maxWidth="md" style={{ marginTop: 30 }}>
      <Grid
        container
        direction="column"
        justify="flex-start"
        alignItems="stretch"
        spacing={1}
      >
        <Grid item xs={12}>
          <Typography variant="h3" component="h1" gutterBottom>
            <span
              role="img"
              aria-label="Cooking woman"
              style={{ marginRight: 10 }}
            >
              👩‍🍳
            </span>
            Greta's Proportions App
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <TextField
              onChange={chageTextarea}
              value={recipe}
              rows={15}
              multiline
              placeholder="Inser your recipe"
            />
          </FormControl>
        </Grid>
        {ingridients.map((ingridient, key) => (
          <Grid key={key} item>
            <Card>
              <CardContent>
                <TextField
                  className={classes.big}
                  id={`${ingridient.name}${key}-tot`}
                  label="Grammature"
                  variant="outlined"
                  value={
                    pivot && pivot.name === ingridient.name
                      ? ingridient.tot
                      : renderWithPivot(ingridient.tot)
                  }
                  error={ingridient.error === "tot"}
                  onChange={(e) =>
                    updateIndexIngridient(key, "tot", Number(e.target.value))
                  }
                />
                <Select
                  className={classes.small}
                  labelId={`${ingridient.name}${key}-unit`}
                  id={`${ingridient.name}${key}-unit`}
                  value={ingridient.unit}
                  onChange={(e) =>
                    updateIndexIngridient(key, "unit", e.target.value)
                  }
                  error={ingridient.error === "unit"}
                >
                  <MenuItem value={"g"}>g</MenuItem>
                  <MenuItem value={"kg"}>Kg</MenuItem>
                </Select>
                <TextField
                  className={classes.big}
                  id={`${ingridient.name}${key}-name`}
                  label="Ingridient name"
                  value={ingridient.name}
                  error={ingridient.error === "name"}
                  onChange={(e) =>
                    updateIndexIngridient(key, "name", e.target.value)
                  }
                />
                <Switch
                  className={classes.small}
                  checked={pivot && pivot.name === ingridient.name}
                  onChange={(e) => {
                    if (pivot && pivot.name === ingridient.name) {
                      lastPivot = null;
                      setPivot(null);
                    } else {
                      lastPivot = { ...ingridient };
                      setPivot(ingridient);
                    }
                  }}
                  color="primary"
                  inputProps={{ "aria-label": "primary checkbox" }}
                />
              </CardContent>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
}

export default App;
